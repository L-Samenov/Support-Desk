import { search } from './search'
import * as actionTypes from './actionTypes';

describe('Profile action', () => {
  it('should create an action to add a todo', () => {
    const filter = 'text'
    const expectedAction = {
      type: actionTypes.SEARCH,
      filter
    }
    expect(search(filter)).toEqual(expectedAction)
  })
})