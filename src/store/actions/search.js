import * as actionTypes from './actionTypes';

export const search = ( filter ) => {
    return {
        type: actionTypes.SEARCH,
        filter
    };
};