import { setProfiles } from './profiles'
import * as actionTypes from './actionTypes';

describe('Profile action', () => {
  it('should create an action to add a todo', () => {
    const profiles = {firstName: 'Maverik', lastName:'Born'}
    const expectedAction = {
      type: actionTypes.SET_PROFILES,
      profiles
    }
    expect(setProfiles(profiles)).toEqual(expectedAction)
  })
})