import * as actionTypes from './actionTypes';
import * as profiles from '../../assets/api.json';
import { setAllProfiles } from '../../utility';

export const setProfiles = ( profiles ) => {
    return {
        type: actionTypes.SET_PROFILES,
        profiles
    };
};

export const fetchProfilesFailed = () => {
    return {
        type: actionTypes.FETCH_PROFILES_FAILED
    };
};

export const initProfiles = () => {
    return dispatch => {

        //If real back-end it can be done in this way:
            // fetch('some-url')
            //     .then( response => {
            //         response.json();
            //     })
            //     .then(data => {
            //         dispatch(setProfiles(data));
            //     })
            //     .catch( error => {
            //         dispatch(fetchProfilesFailed());
            //     } );
        //    
        setAllProfiles(profiles.default);
        profiles.default.length > 0 ? dispatch(setProfiles(profiles.default)):  dispatch(fetchProfilesFailed());
       ;
    };
};