import * as actionTypes from '../actions/actionTypes';
import { getAllProfiles } from '../../utility';

const initialState = {
    profiles:[],
    error: false
};


const setProfiles = ( state, action ) => {
    return {
        ...state,
        profiles:action.profiles
    }
};

const fetchProfilesFailed = (state, action) => {
    return {
        ...state,
        error:true
    }
};


const search = (state, action) => {
    let filtered;
    const allProfiles = getAllProfiles();
    filtered = allProfiles.filter( profile => profile.firstName.toLowerCase().indexOf(action.filter.toLowerCase()) > -1);
    return {
        ...state,
        profiles:filtered
    }
};


const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.SET_PROFILES: return setProfiles( state, action );
        case actionTypes.FETCH_PROFILES_FAILED: return fetchProfilesFailed( state, action );
        case actionTypes.SEARCH: return search( state, action );
        default: return state;
    }
};


export default reducer;