import reducer from './profiles';
import * as actionTypes from '../actions/actionTypes';

describe('profiles reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            profiles:[],
            error: false
        });
    });

    it('should store profiles', () => {
        expect(reducer({
            profiles:[],
            error: false
        }, { 
            type: actionTypes.SET_PROFILES,
            profiles:[{firstName: 'Maverik', lastName:'Born'}]
         })).toEqual({
            profiles:[{firstName: 'Maverik', lastName:'Born'}],
            error: false
        });
    })
});
