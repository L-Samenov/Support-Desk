import React from 'react';
import Layout from './components/Layout/Layout';
import { Route, Switch } from 'react-router-dom';
import Profiles from './components/Profiles/Profiles';

const App = () => {


  //Switch can be ommited for just one route but it's here for future updates with more routes
  const routes = (
    <Switch>
      <Route path="/" component={ Profiles } />                 
    </Switch>
);
    return (
      <div>
        <Layout>
          { routes }
        </Layout>
      </div>
    );
  }



export default App ;
