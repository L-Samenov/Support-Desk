import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Profile from './Profile';
import { Card } from 'react-bootstrap';

configure({adapter: new Adapter()});

const propsObj = {
    profile:{firsName:'Maverik', lastName:'Born'}
}

describe('<Profile />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Profile { ...propsObj}/>);
    });

    it('should render <Profile /> element', () => {
        expect(wrapper.find(Card)).toHaveLength(1);
    });  
});