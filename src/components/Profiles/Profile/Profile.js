import React from 'react';
import { Card } from 'react-bootstrap';
import './Profile.css';
import Available from '../../../assets/icon-available.svg';
import Busy from '../../../assets/icon-busy.svg';
import PropTypes from 'prop-types';


const Profile = (props) => {

    const { firstName, lastName, available, phone, email, image } = props.profile;
    const backgroundStyles = {
        background: available? `url(${ Available })`:`url(${ Busy })`,
        backgroundSize: 'contain'
    }

    return (
        <Card  className="profile-item">
            <div className="img-wrapper">
                <Card.Img variant="top" src={ image } className="profile-img"/>
                <span className="available-icon" style={ backgroundStyles }></span>
            </div>
            <Card.Body>
            <Card.Title>{ firstName } { lastName }</Card.Title>
                <Card.Text className="info">
                    <span>{ phone }</span>
                    <span>{ email }</span>
                </Card.Text>
            </Card.Body>
        </Card>
       
    )
}

Profile.propTypes = {
    profile: PropTypes.shape({
        firstName: PropTypes.string,
        lastName: PropTypes.string,
        available: PropTypes.bool,
        phone: PropTypes.string,
        email: PropTypes.string,
        image: PropTypes.string
    })
  };

export default Profile;