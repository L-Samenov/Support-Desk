import React, { Component, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { connect } from 'react-redux';
import { initProfiles } from '../../store/actions/profiles';
import Profile from './Profile/Profile';
import PropTypes from 'prop-types';
import './Profiles.css';

// class example:

 class Profiles extends Component {

  componentDidMount () {
    this.props.initProfiles();
  }

  render(){
    const { profiles } = this.props.profiles;
    return  (
      <Container className="profiles-wrapper">
        { profiles.length > 0 ? profiles.map((profile) => (
            <Profile key={ profile.email } profile={ profile }/>
           )
          ): null
        }
      </Container>
    )

  }
  
 }


 //  With hooks exaple:

//  const Profiles = ( props ) => {

//     useEffect(() => {
//       props.initProfiles();
//     }, []);

//     const { profiles } = props.profiles;
//     return  (
//       <Container className="profiles-wrapper">
//         { profiles.length > 0 ? profiles.map((profile) => (
//             <Profile key={ profile.email } profile={ profile }/>
//            )
//           ): null
//         }
//       </Container>
//     )

//  }

 Profiles.propTypes = {
  profiles: PropTypes.shape({
    error: PropTypes.bool,
    profiles: PropTypes.arrayOf(PropTypes.shape({
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      available: PropTypes.bool,
      phone: PropTypes.string,
      email: PropTypes.string,
      image: PropTypes.string
    }),)
    
  })
};

 const mapStateToProps = ({ profiles }) => {
  return {
      profiles
  };
}


 export default connect(mapStateToProps, { initProfiles })( Profiles);