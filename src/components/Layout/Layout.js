import React, {Fragment} from 'react';
import Header from '../Header/Header';
import PropTypes from 'prop-types';

const Layout = ({children}) => (
    <Fragment>
        <Header/>
        {children}
    </Fragment>
);

Layout.propTypes = {
    children: PropTypes.element
};

export default Layout;