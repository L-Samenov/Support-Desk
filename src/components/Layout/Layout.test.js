import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Layout from './Layout';

configure({adapter: new Adapter()});

describe('<Layout />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Layout />);
    });

    it('should render <Container /> element', () => {
        expect(wrapper.exists()).toBe(true);
    });

   
});