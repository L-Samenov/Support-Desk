import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Header from './Header';
import Search from '../Search/Search';
import { Nav, Navbar, Container } from 'react-bootstrap';

configure({adapter: new Adapter()});

describe('<Header />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Header />);
    });

    it('should render <Container /> element', () => {
        expect(wrapper.find(Container)).toHaveLength(1);
    });

    it('should render <Nav /> element', () => {
        expect(wrapper.find(Nav)).toHaveLength(1);
    });

    it('should render <Navbar /> element', () => {
        expect(wrapper.find(Navbar)).toHaveLength(1);
    });

    it('should render <Search /> element', () => {
        expect(wrapper.find(Search)).toHaveLength(1);
    });

    

    
});