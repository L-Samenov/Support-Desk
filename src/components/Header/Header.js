import React from 'react';
import Search from '../Search/Search';
import { Nav, Navbar, Container } from 'react-bootstrap';
import './Header.css';

const Header = () => (
    <div className="header-wrapper">
        <Container className="header-container">
            <Navbar expand="lg">
            <Navbar.Brand href="#home" className="brand"> Support Desk</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto"></Nav>
                <Search className="mr-sm-2" />
            </Navbar.Collapse>
            </Navbar>
        </Container>
    </div>
);
export default Header;