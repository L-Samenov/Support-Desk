import React from 'react';
import {connect} from 'react-redux';
import {search} from '../../store/actions/search';
import PropTypes from 'prop-types';
import './Search.css';

const Search = ({ search }) => {
    return (
      <div className="input-wrapper">
        <input
          className="search-input"
          placeholder = "Search"
          onChange={(e) => search(e.target.value)}
        />
        <span className="search-icon"></span>
      </div>
    );
} 

Search.propTypes = {
  search: PropTypes.func
};

export default connect(null, { search })(Search);