import React from 'react';
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Search from './Search';

configure({adapter: new Adapter()});

const mockStore = configureMockStore();
const store = mockStore({});

describe('<Profiles />', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(
            <Provider store={store}>
                <Search/>
            </Provider>
        )
    });

    it('should render <Search />', () => {
        expect(wrapper.exists()).toBe(true);
    });


});